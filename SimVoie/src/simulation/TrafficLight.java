package simulation;
import exception.DEVS_Exception;
import model.*;
import types.*;

public class TrafficLight extends AtomicModel {
	// Definition of S: a state variable is used for the current color

	int etatfeu;
	double dvert=60.0;
	double dorange=5.0;
	double drouge=30.0;
	double sig;
	// 0 pour allume vert
	// 1 pour allume orange
	// 2 pour allume rouge
	// 3 pour etat temporaire avant d'aller en panne
	// 4 pour etat panne
	// 5 pour quitter etat panne � allume
	
	public TrafficLight(String name, String desc) {
		super(name, desc);
		
		// Definition du port de sortie (output:Y):
		String[] colors = { "Green", "Yellow", "Red", "Off"};
		addOutputPortStructure(new DEVS_Enum(colors), this.getName()+".COLOR", "Affichage des signales aux utilisateurs");
		
		// Definition du port d'entr� (input:X):
		addInputPortStructure(new DEVS_Integer(), this.getName()+".MARCHE", "mise en marche du feu");
		
		// State initialization: le feu commence � vert (Green)
		etatfeu = 0;
		sig=0;
	}

	public void deltaInt() {
		// The behavior is the following:
		// allumV -> AllumO -> AllumR -> ...
		if(etatfeu==0){etatfeu =1;sig=dorange;}
		else if(etatfeu==1){etatfeu = 2;sig=drouge;}
		else if(etatfeu==2){etatfeu = 0;sig=dvert;}
		else if(etatfeu==3){etatfeu = 4;sig=DEVS_Real.POSITIVE_INFINITY;}
		else if(etatfeu==5){etatfeu =0 ;sig=dvert;}
	}

	public double ta() {
		return sig;
	}
	public void lambda() throws DEVS_Exception {
		//String toSend = (etatfeu==0)?"Yellow":((etatfeu==1)?"Red":((etatfeu==2)?"Green":(etatfeu==5)?"vert":"Off"));
		String toSend="";
		if(etatfeu==0) toSend="Yellow";
		else if(etatfeu==1) toSend="Red";
		else if(etatfeu==2) toSend="Green";
		else if(etatfeu==5) toSend="Green";
		else toSend="Off";
		setOutputPortData(this.getName()+".COLOR", toSend);
	}

	public void deltaExt(double e) throws DEVS_Exception {
		// Let's get the value received
		int received = ((DEVS_Integer) getInputPortData(this.getName()+".MARCHE")).getInteger();
		
		//o pour off et 1 pour on
		if ((etatfeu==0) && (received == 1)){ etatfeu = 0;sig=sig-dvert;}
		else if ((etatfeu==1) && (received== 1)) {etatfeu = 1;sig=sig-dorange;/*sig=sig-dorange;*/}
		else if ((etatfeu==2) && (received== 1)) {etatfeu = 2;sig=sig-drouge;/*sig=sig-drouge;*/}
		else if ((etatfeu==4) && (received == 1)) {etatfeu = 5;sig=0.0;}
		else if ((etatfeu==0) && (received == 0)) {etatfeu = 3;sig=0.0;}
		else if ((etatfeu==4) && (received== 0)){ etatfeu = 4;sig=0.0;}
		else if ((etatfeu==1) && (received == 0)){ etatfeu = 3;sig=0.0;}
		else if ((etatfeu==2) && (received == 0)) {etatfeu = 3;sig=0.0;}
		else {
			System.out.println("Unexpected value received in " + this.getName() + ":" + received);
			throw new DEVS_Exception();
		}
	}
}