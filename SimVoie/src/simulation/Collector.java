package simulation;
import exception.DEVS_Exception;
import model.*;
import types.*;
import java.io.*;

public class Collector extends AtomicModel {
	// Definition of S :
	PrintStream trajectory;
	
	public Collector(String name, String desc) {
		super(name, desc);
		
		// No output port: Y = {}
		
		// input port X:
		String[] colors = { "Green", "Yellow", "Red", "Off"};
		addInputPortStructure(new DEVS_Enum(colors), this.getName()+".STORE", "Data to store");
		String[] voit={"v"};
		addInputPortStructure(new DEVS_Enum(voit), this.getName()+".STORE2", "Data to store 2");
		// State initialization: the name of the file is xxx.txt if the name of the model is xxx
		try {
			trajectory = new PrintStream(name+".txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public double ta() {
		return DEVS_Real.POSITIVE_INFINITY;
	}
	
	public void deltaInt() {
		// Not defined, since there is a single possible state which time advance value is +infinity
	}

	public void lambda() throws DEVS_Exception {
		// Not defined, for the same reasons
	}

	public void deltaExt(double e) throws DEVS_Exception {
		// Let's get the value received and the simulation time it has been received
		String received = getInputPortData(this.getName()+".STORE").toString();
		String received2 = getInputPortData(this.getName()+".STORE2").toString();
		double when = this.getSimulator().getTL();
		
		// Then store them in the following shape:
		// simulation time : data received
		trajectory.println(when + " : " + received);
		trajectory.println(when + " : " + received2);
	}
}