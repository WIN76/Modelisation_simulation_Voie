package simulation;

import java.util.Random;

import exception.DEVS_Exception;
import model.AtomicModel;
import types.DEVS_Enum;
import types.DEVS_Real;

public class Emetteur extends AtomicModel {
	int code;
	double sig;
	Random rd;
	public Emetteur(String name,String desc){
		super(name,desc);
		//INPUT X
		String[] permission={"true","false"};
		addInputPortStructure(new DEVS_Enum(permission), this.getName()+".INEM", "l'entree de l'emetteur");
		//OUTPUT Y
		String[] car={"v"};
		addOutputPortStructure(new DEVS_Enum(car), this.getName()+".OUTCAR", "la sortie de l'emetteur");
		code=0;
		sig=0;
		rd=new Random();
	}
	@Override
	public void deltaInt(){
		if(code==1) code=1;
	}
	@Override
	public double ta(){
		if(code==0) return sig=DEVS_Real.POSITIVE_INFINITY;
		else return sig=(rd.nextDouble()*3+2);
		
	}
	/**
	 * 
	 */
	@Override
	public void lambda() throws DEVS_Exception {
		String toSend="";
		if(code==1) toSend="v";
		setOutputPortData(this.getName()+".OUTCAR", toSend);

	}
	
	@Override
	public void deltaExt(double e) throws DEVS_Exception {
		String received = getInputPortData(this.getName()+".INEM").toString();
		if((code==0) && (received.compareTo("false")==0)){code=0;sig=DEVS_Real.POSITIVE_INFINITY;}
		else if((code==0) && (received.compareTo("true")==0)){code=1;sig=(rd.nextDouble()*3+2);}
		else if((code==1) && (received.compareTo("false")==0)){code=0;sig=DEVS_Real.POSITIVE_INFINITY;}
		else if((code==1) && (received.compareTo("true")==0)){code=1;sig=sig-e;}
		else {
			System.out.println("Unexpected value received in " + this.getName() + ":" + received);
			throw new DEVS_Exception();
		}
	}


}
