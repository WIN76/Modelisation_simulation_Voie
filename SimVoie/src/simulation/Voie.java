package simulation;

import exception.DEVS_Exception;
import model.AtomicModel;
import types.DEVS_Enum;
import types.DEVS_Integer;
import types.DEVS_Real;

public class Voie extends AtomicModel{
	int etat;
	boolean mem;
	double sig;
	Tableau file=new Tableau();
	public Voie(String name,String desc){
		
		super(name,desc);
		//Definition of the INPUT port ( X)
		String[] car={"v"};
		String[] permission={"true","false"};
		addInputPortStructure(new DEVS_Enum(car), this.getName()+".INCAR", "Entree d'une voiture");
		addInputPortStructure(new DEVS_Enum(permission), this.getName()+".INPERM", "Permission d'entree d'une voiture");
		//Definition of the OUTPUT port (Y) 
		String[] voiture={"v"};
		String[] full={"true","false"};
		addOutputPortStructure(new DEVS_Enum(voiture), this.getName()+".OUTCAR", "sortie d'une voiture");
		addOutputPortStructure(new DEVS_Enum(full), this.getName()+".OUTFULL", "l'etat de la file");
		etat=0;
		mem=true;
		sig=0.0;
	}
	@Override
	public void deltaInt(){
		if(etat==2) etat=1;
		else if(etat==1){
			if(mem==false){
				etat=1;
				file.rapprocher(file.posPrem(file.tabvoie));
			}
			else if((mem==true) && (file.estVide(file.tabvoie)==false)){etat=1;}
			else etat=0;
		}
	}
	@Override
	public double ta(){
		if(etat==0) return sig=DEVS_Real.POSITIVE_INFINITY;
		else if(etat==1) return sig=file.posPrem(file.tabvoie);
		else return sig=0;
		
	}
	/**
	 * 
	 */
	@Override
	public void lambda() throws DEVS_Exception {
		boolean toSend=true;
		String toSendCar="";
		if(etat==2){
			if(file.estVide(file.tabvoie)){
				toSend=true;
			}
			else toSend=false;
		} 
		else if(etat==1) {
			toSend=false;
			toSendCar="v";
			file.decaler(file.posPrem(file.tabvoie));
			}
		setOutputPortData(this.getName()+".OUTFULL", toSend);
		setOutputPortData(this.getName()+".OUTCAR", toSendCar);

	}
	
	@Override
	public void deltaExt(double e) throws DEVS_Exception {
		String receivedCar = getInputPortData(this.getName()+".INCAR").toString();//reception d'une voiture
		String receivedperm = getInputPortData(this.getName()+".INPERM").toString();//reception d'une permission
		if((etat==0) && ((receivedperm.compareTo("true")==0)||(receivedperm.compareTo("false"))==0)){etat=0;sig=DEVS_Real.POSITIVE_INFINITY;}
		else if((etat==0) && (receivedCar.compareTo("v")==0)){etat=2;file.addCar();}
		else if((etat==1) && (receivedperm.compareTo("true")==0)){etat=1;mem=true;file.decaler((int)e);}
		else if((etat==1) && (receivedperm.compareTo("false")==0)){etat=1;mem=false;file.decaler((int)e);}
		else if((etat==1) &&(receivedCar.compareTo("v")==0)){etat=2;file.decaler((int)e);file.addCar();}
		else {
			System.out.println("Unexpected value received in " + this.getName() + ":" + receivedCar);
			System.out.println("Unexpected value received in " + this.getName() + ":" + receivedperm);
			throw new DEVS_Exception();
		}
		
	}
	


}
