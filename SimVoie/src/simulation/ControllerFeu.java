package simulation;

import java.util.Random;

import exception.DEVS_Exception;
import model.AtomicModel;
import types.*;

public class ControllerFeu extends AtomicModel {
	int etatC;
	double duree;
	Random dn;
	/**
	 * 0 pour dire que le controller ne fait rien
	 * 1 pour dire qu'il est entraine d'agir
	 * */
	public ControllerFeu(String name, String desc){
		super(name,desc);
		//determiner les entr�es
		String sig[]={"Green","Yellow","Red","Off"};
		addInputPortStructure(new DEVS_Enum(sig),this.getName()+".SIGNAL","Entree");
		//les sorties
		addOutputPortStructure(new DEVS_Integer(), this.getName()+".CONTROL", "sortie");
		//initialisation
		etatC=0;
		duree=DEVS_Real.POSITIVE_INFINITY;
		dn=new Random();
		}
	@Override
	public void deltaInt(){
		//a l'etat 1 qui est agit si on ne le derange pas il passe � l'etat neant
		if(etatC==1) etatC=0;
				
	}
	@Override
	public double ta(){
		if(etatC==0)
			return duree=DEVS_Real.POSITIVE_INFINITY;
		else
			return duree=(dn.nextDouble()*8 + 2.0);
		
	}
	
	@Override
	public void lambda() throws DEVS_Exception {
		DEVS_Integer toSend =new DEVS_Integer(1);
		setOutputPortData(this.getName()+".CONTROL", toSend);
	}
	@Override
	public void deltaExt(double e) throws DEVS_Exception {
		// TODO Auto-generated method stub
		String received = getInputPortData(this.getName()+".SIGNAL").toString();
		if((etatC==0) && (received.compareTo("Green") ==0)) etatC=0;
		else if((etatC==0) &&(received.compareTo("Yellow") ==0)) etatC=0;
		else if((etatC==0) && (received.compareTo("Red") ==0)) etatC=0;
		else if((etatC==0) && (received.compareTo("Off") ==0)) etatC=1;
		else{
			System.out.println("Unexpected value received in " + this.getName() + ":" + received);
			throw new DEVS_Exception();
		}
		
	}
}
