package simulation;

import java.util.ArrayList;
import model.CoupledModel;
import model.Model;

public class GlobalSystem extends CoupledModel {
	public GlobalSystem(String name,String desc){
		super(name,desc);
		AleaFeu alea=new AleaFeu("ALEA","envoie periodique");
		Collector collecte=new Collector("SLINK", " ");
		ControllerFeu control=new ControllerFeu("TARGET"," ");
		Emetteur emet=new Emetteur("EMET", "");
		TrafficLight trafic=new TrafficLight("SOURCE"," ");
		Transformateur trans=new Transformateur("TRANS", "");
		Voie voie=new Voie("VOIE", "");
		addSubModel(alea);
		addSubModel(collecte);
		addSubModel(control);
		addSubModel(emet);
		addSubModel(trafic);
		addSubModel(trans);
		addSubModel(voie);
		
		//la sortie de l'alea => l'entree du feu
		addIC(alea.getOutputPortStructure("ALEA.OUT"), trafic.getInputPortStructure("SOURCE.MARCHE"));
		//la sortie de controlleur de feu => l'entree du feu
		addIC(control.getOutputPortStructure("TARGET.CONTROL"), trafic.getInputPortStructure("SOURCE.MARCHE"));
		//la sortie du feu =>l'entree du controlleur de feu
		addIC(trafic.getOutputPortStructure("SOURCE.COLOR"), control.getInputPortStructure("TARGET.SIGNAL")) ;
		//la sortie du feu => l'entree du transformateur
		addIC(trafic.getOutputPortStructure("SOURCE.COLOR"), trans.getInputPortStructure("TRANS.INTRANS")) ;
		//la sortie du transformateur => l'entree de la voie
		addIC(trans.getOutputPortStructure("TRANS.OUTTRANS"), voie.getInputPortStructure("VOIE.INPERM")) ;
		//la sortie du l'emetteur => l'entree de la voie
		addIC(emet.getOutputPortStructure("EMET.OUTCAR"), voie.getInputPortStructure("VOIE.INCAR")) ;
		//la sortie de la voie =>l'entree de l'emetteur
		addIC(voie.getOutputPortStructure("VOIE.OUTFULL"), emet.getInputPortStructure("EMET.INEM")) ;
		//la sortie de la voie => l'entree du collector
		//la sortie du feu => l'entree du collector
		
		
	}
	@Override
	public Model select(ArrayList<Model> autoriseModel) {
		// TODO Auto-generated method stub
		for ( Model m : autoriseModel ) {
			if ( m instanceof AleaFeu ) return m ;
		}
		return autoriseModel.get(0);
	}
}
