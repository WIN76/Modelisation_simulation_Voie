package simulation;

import java.util.Random;

import exception.DEVS_Exception;
import model.AtomicModel;
import types.DEVS_Enum;
import types.DEVS_Real;

public class Transformateur extends AtomicModel{
	int etatTrans;
	double sig;
	Random rd;
	public Transformateur(String name,String desc){
		super(name,desc);
		// Definition of the input port (for X):
		String[] colors = { "Green", "Yellow", "Red", "Off"};
		addInputPortStructure(new DEVS_Enum(colors), this.getName()+".INTRANS", "le port d'entree du transformateur");
		// Definition of the output port (for Y):
		String[] autorisation={ "true", "false"};
		addOutputPortStructure(new DEVS_Enum(autorisation), this.getName()+".OUTTRANS", "le port de sortie du transformateur");
		etatTrans=0;
		sig=0;
		rd=new Random();
	}
	@Override
	public void deltaInt(){
		if(etatTrans==1) etatTrans=1;
		else etatTrans=0;
		
	}
	@Override
	public double ta(){
		if(etatTrans==0) return sig=(rd.nextDouble()*2+3);
		else return sig=(rd.nextDouble()*2+3);
	}
	/**
	 * 
	 */
	@Override
	public void lambda() throws DEVS_Exception {
	boolean toSend=true;
	if(etatTrans==1)  toSend=false;
	else toSend=true;
	setOutputPortData(this.getName()+".OUTTRANS", toSend);

	}
	
	@Override
	public void deltaExt(double e) throws DEVS_Exception {
		String received = getInputPortData(this.getName()+".INTRANS").toString();
		if((etatTrans==0) && (received.compareTo("Green")==0)){etatTrans=0; sig=sig-e;}
		else if((etatTrans==0) && (received.compareTo("Yellow")==0)){etatTrans=0;sig=sig-e;}
		else if((etatTrans==0) && (received.compareTo("off")==0)){etatTrans=0;sig=sig-e;}
		else if((etatTrans==0) && (received.compareTo("Red")==0)){etatTrans=1;sig=sig-e;}
		else if((etatTrans==1) && (received.compareTo("Green")==0)){etatTrans=0;sig=sig-e;}
		else if((etatTrans==1) && (received.compareTo("Yellow")==0)){etatTrans=0;sig=sig-e;}
		else if((etatTrans==1) && (received.compareTo("off")==0)){etatTrans=0;sig=sig-e;}
		else {
			System.out.println("Unexpected value received in " + this.getName() + ":" +received);
			throw new DEVS_Exception();
		}

	}


}
