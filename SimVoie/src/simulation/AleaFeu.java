package simulation;

import java.util.Random;

import exception.DEVS_Exception;
import model.AtomicModel;
import types.*;

public class AleaFeu extends AtomicModel {
	Random rd;
	public AleaFeu(String name,String desc){
		super(name,desc);
		//Definition du port de sortie(output Y=0);
		addOutputPortStructure(new DEVS_Integer(), this.getName()+".OUT", "My single output port");
		//initialisation
		rd = new Random();
	}
	@Override
	public void deltaInt(){
	}
	@Override
	public double ta(){
		//intervalle 5 100
		return (rd.nextDouble()*96 + 5.0);
	}
	//il n'ya qu'une sortie
	@Override
	public void lambda() throws DEVS_Exception {
		DEVS_Integer toSend=new DEVS_Integer(0);
		setOutputPortData(this.getName()+".OUT", toSend);

	}
	//il n'y a pas d'entree
	@Override
	public void deltaExt(double e) throws DEVS_Exception {

	}
}
